import time
import serial
import Adafruit_CharLCD as LCD
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

lcd_columns= 16
lcd_rows= 2
lcd_rs = 27
lcd_en = 22
lcd_d4 = 25
lcd_d5 = 24
lcd_d6 = 23
lcd_d7 = 18
lcd_backlight = 4
lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows, lcd_backlight, enable_pwm=True)

lcd.set_backlight(0.5);

ser = serial.Serial('/dev/ttyS0',9600)
try:
    while True:
            reading = ser.readline()
            lcd.message('Temp : ')
            lcd.message(reading[:-2].decode())
            time.sleep(0.5)
            lcd.clear()

except KeyboardInterrupt:
    GPIO.cleanup()
